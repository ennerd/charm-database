<?php
declare(strict_types=1);

namespace Charm\DB;

class Error extends \Charm\Error
{
    protected string $reasonPhrase = 'Internal Database Error';
}
