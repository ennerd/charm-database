<?php
declare(strict_types=1);

namespace Charm;

use PDO;
use Exception;
use Generator;
use PDOException;
use PDOStatement;
use Charm\DB\Error;
use ReflectionClass;
use Charm\Context\Context;

/**
 * Charm\DB
 * ========.
 *
 * Charm\DB is a very thin PDO wrapper. I have written this wrapper several times, and
 * used it in many different projects, because it doesn't try to make things complex,
 * and it doesn't attempt to create a new "easier to use" version of SQL:
 *
 *  * It transparently creates prepared statements (and caches them).
 *
 *  * It makes it very natural to bind variables in your queries. It is still
 *    possible to use use `$db->quote()` should you make a qualified decision
 *    to do so - but remember that this makes your query string unique (so the
 *    prepared statements cache will still grow).
 *
 *  * It always buffers the result from the database. This was a lesson learned
 *    from a large scale project, where it became an issue that the "cursor"
 *    wasn't closed - causing deadlocks, and also it is costly for the database
 *    to have a lot of data queued. If this is an issue, then you should consider
 *    running some queries the "low level" way via direct access to `$db->pdo()`.
 *
 *
 * Instuctions
 * -----------
 *
 * If you are using charm\db inside another project, you don't have to configure
 * the database connection. This is what you would do:
 *
 * ```
 * Charm\DB::$instance = $someExistingPDOInstance;
 * ```
 *
 * Otherwise, you should provide a PDO DSN string, a username and a password.
 * Consult the PHP documentation for the DSN string for other database engines.
 *
 * ```
 * Charm\DB::$dsn = 'sqlite:/var/www/database.sqlite';
 * Charm\DB::$username = 'some-username';
 * Charm\DB::$password = 'some-password';
 * ```
 *
 * If you don't want to pre-configure the database adapter, or you wish to configure
 * it into some Service Container, the constructor can be used directly:
 *
 * ```
 * $db = new Charm\DB($dsn, $username, $password, $options)
 * ```
 *
 *
 * Querying data
 * -------------
 *
 * Database queries are done through either `$db->query()` or `$db->column()`.
 *
 * ```
 * // Multiple rows returns an iterable
 * $users = $db->query('SELECT * FROM users WHERE last_login < ?', [gmdate('Y-m-d H:i:s')]);
 *
 * // Single rows can be fetched this way (sending 'LIMIT 1' is kind to your database engine)
 * $user = $db->query('SELECT * FROM users WHERE username=? LIMIT 1', [ $username ])->current();
 *
 * // Fetch a single column
 * $usernames = $db->column('SELECT username FROM users);
 *
 * // Fetcha single value
 * $numberOfUsers = $db->column('SELECT COUNT(*) FROM users')->current();
 * ```
 *
 *
 * Definition, Manipulation, Control
 * ---------------------------------
 *
 * All modifications to the database are done through the `$db->exec()` function.
 *
 * ```
 * // Insert a row into a table
 * if ($db->exec('INSERT INTO users (username, password ) VALUES (?, ?)', [ $username, $password ])) {
 *     echo "Created a new user with id ".$db->lastInsertId()."!\n";
 * }
 *
 * // Delete some data
 * if ($count = $db->exec('DELETE FROM users WHERE expires_date < ?', [ gmdate('Y-m-d H:i:s') ])) {
 *     echo "Deleted $count users\n";
 * }
 * ```
 *
 *
 * Error Handling
 * --------------
 *
 * The wrapper defaults to `PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION`, so you'll be receiving
 * standard PDO exceptions.
 *
 * Invalid usage of this wrapper will trigger `Charm\DB\Error` exceptions with error codes
 * according to the class constants. If you're not using `PDO::ERRMODE_EXCEPTION` we'll only
 * throw `Charm\DB\Error` exceptions.
 *
 * The query issued when the last error was triggered is available in `$db->lastFailedQuery`.
 *
 *
 * Security
 * --------
 *
 * This class does NOTHING for security, but it makes it easier for YOU to do more secure
 * database queries.
 *
 * This class overrides the `__debugInfo()` magic method to avoid accidentally exposing secrets,
 * but in general you should NEVER display raw error messages in production.
 */
class DB
{
    // Used if the PDO instance somehow is configured to not throw exceptions:
    public const INVALID_ARGUMENT_ERROR = 1;
    public const INVALID_QUERY_ERROR = 2;
    public const INVALID_STATEMENT_ERROR = 3;
    public const BEGIN_TRANSACTION_ERROR = 4;
    public const COMMIT_TRANSACTION_ERROR = 5;
    public const ROLLBACK_TRANSACTION_ERROR = 6;
    public const NOT_CONNECTED_ERROR = 7;
    public const DATABASE_RELATED_ERROR = 8;

    /**
     * Config options.
     */
    public static ?DB $instance = null;
    public static ?string $dsn = 'sqlite::memory:';
    public static ?string $username = null;
    public static ?string $password = null;
    public static ?array $options = [
        /*
            * The MySQL init command can use :timezone as an alias. The value of :timezone
            * is extracted from calling `date('P')` when the connection is established.
            */
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone=':timezone'; SET NAMES utf8mb4;",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // Holds the last failed query when an exception is thrown
    public ?string $lastFailedQuery;

    // The PDO instance
    protected ?PDO $pdo;

    // The statement cache
    protected array $stmts = [];

    // The last insert id
    protected mixed $_lastInsertId;

    public static function getConfig(): array
    {
        static $config;
        if ($config) {
            return $config;
        }

        return Hooks::instance()->filter(__METHOD__, [
            'dsn' => static::$dsn,
            'username' => static::$username,
            'password' => static::$password,
            'options' => static::$options,
        ]);
    }

    /**
     * Views applied to query() using CTEs.
     *
     * @var array<string, array{0: string, 1: ?array}>
     */
    protected $views = [];

    /**
     * Return a new instance of `Charm\DB` which will apply views to tables before running the DB::query() method using
     * common table expressions (CTEs).
     *
     * Example: $db->withView('users', 'EXISTS SELECT 1 FROM client_users WHERE users.id=client_users.user_id AND client_users.client_id=?)', [$clientId]);
     *
     * @return self
     */
    public function withView(string $viewName, string $query, array $vars)
    {
        if (isset($this->views[$viewName])) {
            throw new Error("Can't redeclare view '$viewName'");
        }
        $clone = clone $this;
        $clone->views[$viewName] = [$query, $vars];

        return $clone;
    }

    public function getViews(): array
    {
        $views = Hooks::instance()->filter(__METHOD__, $this->views);
        foreach ($views as $view) {
            if (\is_string($view[0]) || !\is_array($view[1])) {
                throw new Error(__METHOD__.' filter expects array<string, array{0: string, 1: array}>');
            }
        }

        return $views;
    }

    /**
     * Returns a connection to the database. The connection is stored in the context and
     * reused for the context.
     * 
     * @return self
     */
    public static function instance(): self
    {
        $ctx = Context::instance();
        if (isset($ctx->db)) {
            return Hooks::instance()->filter(__METHOD__, $ctx->db);
        }

        $config = static::getConfig();
        /*
         * Apply replacement variables to the options array.
         * Character set should also be specified.
         */
        if (isset($config['options'][PDO::MYSQL_ATTR_INIT_COMMAND])) {
            $config['options'][PDO::MYSQL_ATTR_INIT_COMMAND] = strtr($config['options'][PDO::MYSQL_ATTR_INIT_COMMAND], [':timezone' => date('P')]);
        }

        $ctx->instance = new self($config['dsn'], $config['username'], $config['password'], $config['options']);

        return Hooks::instance()->filter(__METHOD__, $ctx->instance);
    }

    /**
     * Wrap an existing PDO instance, so that you can use the `Charm\DB` API
     * without creating a new database connection.
     */
    public static function wrap(PDO $pdo): self
    {
        $r = new ReflectionClass(static::class);
        $db = $r->newInstanceWithoutConstructor();
        $db->pdo = $pdo;

        return $db;
    }

    /**
     * {@see PDO}.
     */
    public function __construct(string $dsn, string $username = null, string $password = null, array $options = null)
    {
        try {
            $this->pdo = new PDO($dsn, $username, $password, $options);
        } catch (PDOException $e) {
            throw new Error($e->getMessage(), $e->getCode(), $e, [500, 'Internal Server Error', 'dsn' => $dsn, 'username' => $username, 'password' => '*REMOVED*', 'options' => $options]);
        }
    }

    /**
     * Provides direct access to the `PDO` instance.
     */
    public function pdo(): ?PDO
    {
        return $this->pdo;
    }

    /**
     * Releases the PDO connection, and optionally replaces it with
     * a new connection. Unless you keep the returned PDO instance,
     * this will close the previous connection.
     *
     * Note that if you try to perform queries on this object, when it
     * doesn't have a PDO instance, lot's of errors will occur because
     * it isn't worthwhile to check if we have a PDO instance.
     */
    public function close(PDO $newConnection = null): ?PDO
    {
        $result = $this->pdo;
        $this->pdo = $newConnection;
        $this->stmts = [];

        return $result;
    }

    /**
     * Perform a database select query. Returns an iterator of objects normally.
     *
     * @param int|callable|string $into If number, behaves like `PDO::FETCH_COLUMN`. If callable, behaves like `PDO::FETCH_FUNC`, if string
     *                                  and if the class exists, behaves like `PDO::FETCH_CLASS`. If class does not exist `PDO::FETCH_ASSOC`.
     */
    public function query(string $query, array $vars = [], $into = null): Generator
    {
        $finalVars = [];
        /*
                $views = $this->getViews();
                if (\count($views) > 0) {
                    $views = [];
                    foreach ($this->views as $view) {
                        $views[] = $view->name.' AS ('.$view->query.')';
                        foreach ($view->vars as $v) {
                            $finalVars[] = $v;
                        }
                    }
                    $query = 'WITH '.implode(' ', $views).' '.$query;
                }
        */
        foreach ($vars as $k => $v) {
            while (!is_scalar($v) && null !== $v) {
                throw new DB\Error('Argument 2 contains non-scalar values', self::INVALID_ARGUMENT_ERROR);
            }
            $finalVars[] = $v;
        }
        try {
            $stmt = $this->prepare($query);
            $res = $stmt->execute($finalVars);
            $this->debug($query, $finalVars);

            if (!$res) {
                $this->lastFailedQuery = $query;
                throw new DB\Error("Error running query '$query'", self::INVALID_QUERY_ERROR);
            }

            if (null === $into) {
                $rows = $stmt->fetchAll(PDO::FETCH_CLASS);
            } elseif (\is_int($into) && $into >= 0) {
                $rows = $stmt->fetchAll(PDO::FETCH_COLUMN, $into);
            } elseif (\is_callable($into)) {
                $rows = $stmt->fetchAll(PDO::FETCH_FUNC, $into);
            } elseif (class_exists((string) $into)) {
                $rows = $stmt->fetchAll(PDO::FETCH_CLASS, $into);
            } elseif (\is_string($into)) {
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } else {
                throw new DB\Error('Unknown $into argument', static::INVALID_ARGUMENT_ERROR);
            }
            $stmt->closeCursor();
        } catch (PDOException $e) {
            $this->lastFailedQuery = $query;
            throw new Error($e->getMessage(), $e->getCode(), $e, [500, 'Internal Server Error', 'query' => $query, 'vars' => $finalVars, 'into' => $into]);
        }
        foreach ($rows as $row) {
            yield $row;
        }
    }

    /**
     * Execute a simple statement. Works like `PDO::exec` but uses a prepared
     * statement and returns the affected row count. Note that the affected rows
     * number will be 0 with other statements than INSERT, UPDATE, DELETE.
     *
     * If an error occurs, an exception is thrown.
     */
    public function exec(string $query, array $vars = []): int
    {
        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }

        try {
            $stmt = $this->prepare($query);
            $res = $stmt->execute($vars);
            $this->debug($query, $vars);

            if ($res) {
                $this->_lastInsertId = $this->pdo->lastInsertId();

                return $stmt->rowCount();
            }
            $this->_lastInsertId = null;
            $this->lastFailedQuery = $query;
            throw new DB\Error("Error running statement '$query'", self::INVALID_STATEMENT_ERROR);
        } catch (PDOException $e) {
            $this->_lastInsertId = null;
            $this->lastFailedQuery = $query;
            throw new Error($e->getMessage(), $e->getCode(), $e, [500, 'Internal Server Error', 'query' => $query, 'vars' => $vars]);
        }
    }

    /**
     * Run an insert statement based on keys and values from an object/array,
     * and return the last insert id. You can limit which columns are used by providing
     * the `$keys` array. Any non-scalar values and their keys will be removed.
     *
     * @param string       $table     The name of the table
     * @param object|array $values    Object or array with keys and values
     * @param array        $keys=null Optional array with key names
     * @returns mixed               The last inserted ID (probably an int)
     */
    public function insert(string $table, $values, array $keys = null): ?string
    {
        list($keys, $vals) = $this->getKeyVals($values, $keys);

        $stmt = 'INSERT INTO '.$this->quoteIdentifier($table).' ('.implode(',', array_map([$this, 'quoteIdentifier'], $keys)).') VALUES ('.implode(',', array_fill(0, \count($keys), '?')).')';

        try {
            $result = $this->exec($stmt, $vals);

            if (false === $result) {
                $this->lastFailedQuery = $stmt;
                throw new Error('Unable to insert row in database', self::DATABASE_RELATED_ERROR, null, [500, 'Internal Server Error', 'table' => $table, 'query' => $stmt, 'vars' => $vals]);
            }

            return $this->lastInsertId();
        } catch (PDOException $e) {
            $this->lastFailedQuery = $stmt;
            throw new Error($e->getMessage(), $e->getCode(), $e, [500, 'Internal Server Error', 'table' => $table, 'query' => $stmt, 'vars' => $vals]);
        }
    }

    /**
     * Run a delete statement based oon keys and values from an object/array.
     * Any non-scalar values will be removed.
     *
     * This delete statement compares on all the provided keys, so it can be
     * used to ensure that you're not deleting some data that has changed
     * in the database.
     *
     * @param string       $table   The name of the table
     * @param object|array $values  object or array with keys and values
     * @param int          $limit=1 Limit the number of rows we're deleting. NULL means no limit.
     *
     * @return int The number of rows that were deleted
     */
    public function delete(string $table, $values, ?int $limit = 1)
    {
        list($keys, $vals) = $this->getKeyVals($values);

        $query = 'DELETE FROM '.$this->quoteIdentifier($table).' WHERE '.implode(' AND ', array_map(function ($key) {
            return $this->quoteIdentifier($key).'=?';
        }, $keys));
        if (null !== $limit) {
            $query .= ' LIMIT '.$limit;
        }

        try {
            $result = $this->exec($query, $vals);

            if (false === $result) {
                $this->lastFailedQuery = $query;
                throw new Error('Unable to insert row in database', self::DATABASE_RELATED_ERROR, null, [500, 'Internal Server Error', 'table' => $table, 'query' => $query, 'vars' => $vals]);
            }
        } catch (PDOException $e) {
            $this->lastFailedQuery = $query;
            throw new Error($e->getMessage(), $e->getCode(), $e, [500, 'Internal Server Error', 'query' => $query, 'vars' => $vals]);
        }
    }

    /**
     * Get the primary key generated in the previous insert statement.
     */
    public function lastInsertId(): ?string
    {
        if (!$this->_lastInsertId) {
            return null;
        }

        return $this->_lastInsertId;
    }

    private int $transactionDepth = 0;
    public ?string $transactionTag = null;
    private array $commitCallbacks = [];

    public function onCommit(callable $callback) {
        $this->commitCallbacks[] = $callback;
    }

    public function getTransactionDepth(): int {
        return $this->transactionDepth;
    }

    /**
     * Begin transaction.
     */
    public function beginTransaction()
    {
        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }
        if (0 === $this->transactionDepth) {
            try {
                $result = $this->pdo->beginTransaction();
                if (false === $result) {
                    throw new DB\Error('Unable to begin transaction', static::BEGIN_TRANSACTION_ERROR);
                }
                ++$this->transactionDepth;
                $this->transactionTag = microtime(true).mt_rand(1000000, 9999999);
            } catch (PDOException $e) {
                throw new Error($e->getMessage(), self::BEGIN_TRANSACTION_ERROR, $e, [500, 'Internal Server Error', 'query' => 'BEGIN TRANSACTION']);
            }
        } else {
            try {
                $result = $this->pdo->exec($sql = 'SAVEPOINT transaction_'.$this->transactionDepth);
                if (false === $result) {
                    throw new DB\Error('Unable to create nested transaction using SAVEPOINT', self::BEGIN_TRANSACTION_ERROR);
                }
                ++$this->transactionDepth;
            } catch (PDOException $e) {
                throw new Error($e->getMessage(), self::BEGIN_TRANSACTION_ERROR, $e, [500, 'Internal server error', 'query' => 'SAVEPOINT transaction_'.$this->transactionDepth]);
            }
        }
    }

    /**
     * Rollback transaction.
     */
    public function rollBack(): void
    {
        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }
        if (0 === $this->transactionDepth) {
            throw new Error('No current transaction when calling DB::rollBack()', static::ROLLBACK_TRANSACTION_ERROR);
        } elseif (1 === $this->transactionDepth) {
            try {
                $result = $this->pdo->rollBack();
                if (false === $result) {
                    throw new DB\Error('Unable to rollback transaction', static::ROLLBACK_TRANSACTION_ERROR);
                }
                --$this->transactionDepth;
                $this->transactionTag = null;
            } catch (PDOException $e) {
                throw new Error($e->getMessage(), self::ROLLBACK_TRANSACTION_ERROR, $e, [500, 'Internal Server Error', 'query' => 'ROLLBACK', 'vars' => null]);
            }
        } else {
            try {
                $result = $this->pdo->exec('ROLLBACK TO transaction_'.($this->transactionDepth - 1));
                if (false === $result) {
                    throw new DB\Error('Unable to rollback nested transaction', self::ROLLBACK_TRANSACTION_ERROR);
                }
                --$this->transactionDepth;
            } catch (PDOException $e) {
                throw new Error($e->getMessage(), self::ROLLBACK_TRANSACTION_ERROR, $e, [500, 'Internal Server Error', 'query' => 'ROLLBACK TO transaction_'.($this->transactionDepth - 1)]);
            }
        }
    }

    /**
     * Commit transaction.
     */
    public function commit(): void
    {
        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }
        if (0 === $this->transactionDepth) {
            throw new Error('No transaction to commit', self::COMMIT_TRANSACTION_ERROR, null, [500, 'Internal Server Error']);
        } elseif (1 === $this->transactionDepth) {
            try {
                $result = $this->pdo->commit();
                if (!$result) {
                    throw new DB\Error('Unable to commit transaction', static::COMMIT_TRANSACTION_ERROR);
                }
                --$this->transactionDepth;
                $this->transactionTag = null;
                foreach ($this->commitCallbacks as $callback) {
                    $callback();
                }
            } catch (PDOException $e) {
                throw new Error($e->getMessage(), self::COMMIT_TRANSACTION_ERROR, $e, [500, 'Internal Server Error', 'query' => 'COMMIT']);
            }
        } else {
            try {
                $result = $this->pdo->exec('RELEASE SAVEPOINT transaction_'.($this->transactionDepth - 1));

                if (false === $result) {
                    throw new DB\Error('Unable to commit nested transaction', self::COMMIT_TRANSACTION_ERROR);
                }
                --$this->transactionDepth;
            } catch (PDOException $e) {
                throw new Error($e->getMessage(), self::COMMIT_TRANSACTION_ERROR, $e, [500, 'Internal Server Error', 'query' => 'RELEASE SAVEPOINT transaction_'.($this->transactionDepth - 1)]);
            }
        }
    }

    /**
     * Quote a string for use in the database. Normally this isn't used when using prepared statements, but in
     * a few circumstances it can be helpful.
     */
    public function quote($value): string
    {
        if (\is_int($value) || \is_float($value)) {
            return (string) $value;
        } elseif (\is_bool($value)) {
            return $value ? '1' : '0';
        } elseif (null === $value) {
            return 'NULL';
        }

        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }

        return $this->pdo->quote($value);
    }

    /**
     * Quoting identifiers differs from quoting strings and values in most
     * database engines.
     */
    public function quoteIdentifier(string $value): string
    {
        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }

        switch ($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME)) {
            // Special cases
            case 'mysql':
                return implode('.', array_map(function ($value) { return '`'.strtr($value, ['`' => '``']).'`'; }, explode('.', $value)));

            case 'sqlsrv':
                return implode('.', array_map(function ($value) { return '['.strtr($value, [']' => '][']).']'; }, explode('.', $value)));

            // Most other cases
            default:
                return implode('.', array_map(function ($value) { return '"'.strtr($value, ['"' => '""']).'"'; }, explode('.', $value)));
        }
    }

    /**
     * Fetch a cached statement or prepare a new one. Can be used to perform
     * special variations of queries like `PDO::FETCH_COLUMN|PDO::FETCH_GROUP`
     * etc.
     *
     * If you use `$driver_options`, your statement will not be cached.
     */
    public function prepare(string $stmt, array $driver_options = null): PDOStatement
    {
        if (!$this->pdo) {
            throw new DB\Error('Not connected', self::NOT_CONNECTED_ERROR);
        }
        try {
            if (null !== $driver_options) {
                return $this->pdo->prepare($stmt, $driver_options);
            }
            if (\array_key_exists($stmt, $this->stmts)) {
                return $this->stmts[$stmt];
            }

            return $this->stmts[$stmt] = $this->pdo->prepare($stmt);
        } catch (PDOException $e) {
            throw new Error($e->getMessage(), $e->getCode(), $e, [500, 'Internal Server Error', 'query' => $stmt, 'driver_options' => $driver_options]);
        }
    }

    /**
     * Helper function that takes keys and values from an array or an object
     * and returns two arrays containing the keys and the values. Only values
     * that are of scalar types are kept.
     *
     * Can also filter values according to a list of keys.
     *
     * @param object|array $values Holds the values
     * @param array        $keys   = null    To filter the keys
     */
    protected function getKeyVals($values, array $keys = null): array
    {
        if (\is_object($values)) {
            $values = get_object_vars($values);
        } elseif (!\is_array($values)) {
            throw new DB\Error('Expecting an array or an object', static::INVALID_ARGUMENT_ERROR);
        }

        if (null === $keys) {
            $keys = array_keys($values);
        }

        $vals = [];
        foreach ($keys as $key) {
            if (\array_key_exists($key, $values) && !is_scalar($values[$key])) {
                continue;
            }
            $vals[] = $values[$key] ?? null;
        }

        return [$keys, $vals];
    }

    private function debug(string $sql, array $vars)
    {
        if (!(\defined('DEBUG') && \constant('DEBUG'))) {
            return;
        }
        foreach ($vars as $val) {
            $pos = strpos($sql, '?');
            if (false !== ($pos = strpos($sql, '?'))) {
                $sql = substr($sql, 0, $pos).'{{'.strtoupper(\gettype($val)).'}}'.substr($sql, $pos + 1);
            }
        }
        $sql = strtr($sql, ["\n" => '\\n']);
        //header('X-Debug-Log-'.floor(microtime(true) * 1000).": $sql", false);
    }
}
